# Docker Code

This is sample code for setup Laravel project on docker.

# Basic Docker Commands

<ul>
    <li>docker-compose build</li>
    <li>docker compose up</li>
    <li>docker images</li>
    <li>docker ls</li>
    <li>docker exec -it image_name/image_id /bin/sh</li>
</ul>

